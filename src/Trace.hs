{-# LANGUAGE GADTs
           , ExistentialQuantification
           , BangPatterns
           , NamedFieldPuns              #-}

module LF.Trace where

import Data.IORef
import Control.DeepSeq (NFData, deepseq)
import Algebra.Lattice (BoundedJoinSemiLattice, (\/), bottom)
import Control.Concurrent
import GHC.Conc (numCapabilities)
import Control.Monad
import System.IO.Unsafe (unsafePerformIO)

data Trace = Done
           | forall a. (BoundedJoinSemiLattice a) => Get (LVar a) (a -> Bool) (a -> Trace)
           | forall a. (BoundedJoinSemiLattice a) => Put (LVar a) a Trace
           | forall a. (BoundedJoinSemiLattice a) => New a (LVar a -> Trace)
           | forall a. (BoundedJoinSemiLattice a) => Freeze (LVar a) Trace

newtype LVar a = LVar (IORef (Var a))

-- Here we create the LVar contents as a GADT,
-- where `a` must be a `BoundedJoinSemiLattice`
-- and it also stores the continuations for the get function
-- equipped with a threshold set.
-- The threshold set is a predicate, instead of an actual set.
-- The `BoundedJoinSemiLattice` typeclass asks the user
-- to implement the lub operator `\/` and the `bottom` function.
data Var a where
  Var    :: BoundedJoinSemiLattice a => a -> [Trace] -> Var a
  Frozen :: BoundedJoinSemiLattice a => a -> Var a

-- We just have a continuation monad here.
newtype Cont a = Cont { unCont :: (a -> Trace) -> Trace }

instance Functor Cont where
  fmap f a = Cont $ \k -> unCont a (k . f)

instance Applicative Cont where
  pure = return
  (<*>) = ap

instance Monad Cont where
  return a = Cont $ \k -> k a
  m >>= f  = Cont $
    \k -> unCont m (\a -> unCont (f a) k)

-- Only `new`, `get`, `put` and `freeze`.
-- `fork` is not a part of the lambda LVar spec, so we are not implementing it.
new :: BoundedJoinSemiLattice a => a -> Cont (LVar a)
new bot = Cont $ New bot

get :: BoundedJoinSemiLattice a => (a -> Bool) -> LVar a -> Cont a
get p v = Cont $ \k -> Get v p k

put :: (NFData a, BoundedJoinSemiLattice a) => LVar a -> a -> Cont ()
put v a = deepseq a . Cont $ \k -> Put v a (k ())

put_ :: BoundedJoinSemiLattice a => LVar a -> a -> Cont ()
put_ v !a = Cont $ \k -> Put v a (k ())

freeze :: BoundedJoinSemiLattice a => LVar a -> Cont ()
freeze v = Cont $ \k -> Freeze v (k ())

-- Work-stealing scheduler, in the same spirit as in Marlow at al 2011.
-- Only `sched` differs significantly.
data SchedState = SchedState
  { no       :: Int
  , workpool :: IORef [Trace]
  , idle     :: IORef [MVar Bool]
  , scheds   :: [SchedState]      }

reschedule :: SchedState -> IO ()
reschedule state@SchedState{ workpool } =
  (atomicModifyIORef workpool $ \ts ->
      case ts of
        []      -> ([], Nothing)
        (t:ts') -> (ts', Just t))
  >>= \e -> case e of
              Nothing -> steal state
              Just t  -> sched state t


pushWork :: SchedState -> Trace -> IO ()
pushWork SchedState { workpool, idle } t = do
  atomicModifyIORef workpool $ \ts -> (t:ts, ())
  idles <- readIORef idle
  when (not (null idles)) $ do
       r <- atomicModifyIORef idle
         $ \is -> case is of
                    []     -> ([], return ())
                    (j:js) -> (js, putMVar j False)
       r


steal :: SchedState -> IO ()
steal state@SchedState{ idle, scheds, no = my_no } = go scheds
  where
    go (x:xs)
      | no x == my_no = go xs
      | otherwise     = do
          r <- atomicModifyIORef (workpool x) $
            \ts -> case ts of
              []     -> ([], Nothing)
              (y:ys) -> (ys, Just y)
          case r of
            Just t  -> sched state t
            Nothing -> go xs
    go [] = do
      m <- newEmptyMVar
      r <- atomicModifyIORef idle $ \is -> (m:is, is)
      if length r == numCapabilities - 1
        then mapM_ (\n -> putMVar n True) r
        else do done <- takeMVar m
                if done then return ()
                  else go scheds

runPar :: BoundedJoinSemiLattice a => Cont a -> a
runPar x = unsafePerformIO $ do
  let n = numCapabilities
  workpools <- replicateM n $ newIORef []
  is <- newIORef []
  let states = [ SchedState { no = y
                            , workpool = wp
                            , idle = is
                            , scheds = states }
               | (y, wp) <- zip [0..] workpools ]
  m <- newEmptyMVar
  (main, _) <- threadCapability =<< myThreadId
  forM_ (zip [0..] states) $ \(cpu, state) ->
    forkOn cpu $
      if (cpu /= main)
      then reschedule state
      else do
        rref <- newIORef $ Var bottom []
        sched state $
          unCont (x >>= put_ (LVar rref) >> freeze (LVar rref)) (const Done)
        readIORef rref >>= putMVar m
  r <- takeMVar m
  case r of
    Frozen a -> return a
    _        -> error "no result"


-- Scheduler.
sched :: SchedState -> Trace -> IO ()
sched state Done                = reschedule state


-- `New` adds bottom to the LVar
sched state (New bot f) =
  newIORef (Var bot []) >>= \r -> sched state . f $ LVar r


-- `Get` checks whether we are already at the threshold set -
-- either via the predicate or the LVar being frozen -
-- otherwise, adds the whole `Get` trace the LVar blocked traces to the list.
sched state t@(Get (LVar v) p k) =
  readIORef v
  >>= \e -> case e of
              Frozen a -> sched state (k a)
              Var a _  -> if p a then sched state (k a)
                         else (atomicModifyIORef v $ \d ->
                                  case d of
                                    Var b ks -> (Var b (t:ks), reschedule state)
                                    Frozen b -> (Frozen b, sched state (k a))
                              ) >>= id

-- `Put` takes the lub of `a` and `b`,
--  empties the list of continuations from the state,
--  and reschedule all continuations.
sched state (Put (LVar v) b t) =
  (atomicModifyIORef v $ \e ->
      case e of
        Var a ks -> (Var (a \/ b) [], ks)
        Frozen _    -> error "put to a frozen LVar"
  )
  >>= \ks -> mapM_ (pushWork state) ks >> sched state t

sched state (Freeze (LVar v) t) =
  (atomicModifyIORef v $ \e ->
      case e of
        Var a ks -> (Frozen a, ks)
        Frozen a -> (Frozen a, [])
  )
  >>= \ks -> mapM_ (pushWork state) ks >> sched state t
